import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'control-value-accessor';
  data = 3;

  constructor(private fb: FormBuilder) {
    this.form.valueChanges.subscribe(console.info);
  }

  onChange(d: any) {}

  form = this.fb.group({
    name: ['', [Validators.required]],
    github: ['', [Validators.required]],
    website: ['', [Validators.required]],
    server: ['', Validators.required],
    // server: ['IN', Validators.required],
    communications: [
      [
        {
          label: 'Marketing',
          modes: [
            {
              name: 'Email',
              enabled: false,
            },
            {
              name: 'SMS',
              enabled: false,
            },
          ],
        },
        {
          label: 'Product Updates',
          modes: [
            {
              name: 'Email',
              enabled: false,
            },
            {
              name: 'SMS',
              enabled: false,
            },
          ],
        },
      ],
      // [
      //   {
      //     label: 'Marketing',
      //     modes: [
      //       {
      //         name: 'Email',
      //         enabled: true,
      //       },
      //       {
      //         name: 'SMS',
      //         enabled: false,
      //       },
      //     ],
      //   },
      //   {
      //     label: 'Product Updates',
      //     modes: [
      //       {
      //         name: 'Email',
      //         enabled: true,
      //       },
      //       {
      //         name: 'SMS',
      //         enabled: true,
      //       },
      //     ],
      //   },
      // ],
    ],
  });

  toggleServerDisable() {
    // const server = this.form.get('server');
    this.form.disabled ? this.form.enable() : this.form.disable();
  }

  saveForm() {
    this.form;
  }
}
