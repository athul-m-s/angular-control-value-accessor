import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RatingInputComponent } from './rating-input.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommunicationPreferenceModule } from './components/communication-preference/communication-preference.module';
import { CountrySelectorModule } from './components/country-selector/country-selector.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent, RatingInputComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    CommunicationPreferenceModule,
    CountrySelectorModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
